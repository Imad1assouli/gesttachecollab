package com.GestTacheCollab.GestTacheCollab.Model;

import com.GestTacheCollab.GestTacheCollab.Model.Category;
import com.GestTacheCollab.GestTacheCollab.Model.User;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idTask;
    private String title;
    private String description;
    private String status;
    private String dueDate;

    @ManyToOne
    @JoinColumn(name = "created_by")
    private User createdBy;

    @ManyToOne
    @JoinColumn(name = "assigned_to")
    private User assignedTo;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;
}
