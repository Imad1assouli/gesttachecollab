package com.GestTacheCollab.GestTacheCollab.Contoller;

import com.GestTacheCollab.GestTacheCollab.Model.Task;
import com.GestTacheCollab.GestTacheCollab.Model.User;
import com.GestTacheCollab.GestTacheCollab.Service.interfaces.UserService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {
    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/insert")
    public void insertUser(@RequestBody User user) {
        this.userService.insertUser(user);
    }

    @PutMapping("/update")
    public void updateUser(@RequestParam Long id, @RequestBody User user) {
        this.userService.updateUser(id, user);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteUser(@PathVariable Long id) {
        this.userService.deleteUser(id);
    }

    @PostMapping("/{id}/assignTask")
    public void assignTask(@PathVariable Long id, @RequestBody Task task) {
        this.userService.assignTask(id, task);
    }

    @GetMapping("/getUser/{id}")
    public User getUserById(@PathVariable Long id) {
        return this.userService.getUserById(id);
    }
}
