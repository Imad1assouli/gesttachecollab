package com.GestTacheCollab.GestTacheCollab.Contoller;

import com.GestTacheCollab.GestTacheCollab.Model.Task;
import com.GestTacheCollab.GestTacheCollab.Service.interfaces.TaskService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/task")
public class TaskController {
    private final TaskService taskService;

    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @PostMapping("/insert")
    public void insertTask(@RequestBody Task task) {
        this.taskService.insertTask(task);
    }

    @PutMapping("/update")
    public void updateTask(@RequestParam Long id, @RequestBody Task task) {
        this.taskService.updateTask(id, task);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteTask(@PathVariable Long id) {
        this.taskService.deleteTask(id);
    }

    @GetMapping("/allTasks")
    public List<Task> getAllTasks() {
        return this.taskService.getAllTasks();
    }

    @GetMapping("/getTask/{id}")
    public Task getTaskById(@PathVariable Long id) {
        return taskService.getTaskById(id);
    }
}
