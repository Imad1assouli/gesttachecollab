package com.GestTacheCollab.GestTacheCollab.Reposity;

import org.springframework.data.jpa.repository.JpaRepository;
import com.GestTacheCollab.GestTacheCollab.Model.User;

public interface UserReposity extends JpaRepository<User,Long> {
}
