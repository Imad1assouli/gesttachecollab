package com.GestTacheCollab.GestTacheCollab.Reposity;

import org.springframework.data.jpa.repository.JpaRepository;
import com.GestTacheCollab.GestTacheCollab.Model.Category;

public interface CategoryReposity extends JpaRepository<Category,Long> {
}
