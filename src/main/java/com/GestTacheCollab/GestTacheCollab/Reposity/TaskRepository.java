package com.GestTacheCollab.GestTacheCollab.Reposity;

import org.springframework.data.jpa.repository.JpaRepository;
import com.GestTacheCollab.GestTacheCollab.Model.Task;

public interface TaskRepository extends JpaRepository<Task, Long> {
}
