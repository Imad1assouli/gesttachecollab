package com.GestTacheCollab.GestTacheCollab.Service.interfaces;

import com.GestTacheCollab.GestTacheCollab.Model.Task;
import com.GestTacheCollab.GestTacheCollab.Model.User;

public interface UserService {
    void insertUser(User user);
    void deleteUser(Long id);
    void updateUser(Long id,User user);
    void assignTask(Long id, Task task);
    User getUserById(Long id);
}
