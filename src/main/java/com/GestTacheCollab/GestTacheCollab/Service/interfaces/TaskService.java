package com.GestTacheCollab.GestTacheCollab.Service.interfaces;

import com.GestTacheCollab.GestTacheCollab.Model.Task;

import java.util.List;


public interface TaskService {
      void insertTask(Task task);
      void deleteTask(Long id);
      void updateTask(Long id,Task task);
      List<Task> getAllTasks();
      Task getTaskById(Long id);
}
