package com.GestTacheCollab.GestTacheCollab.Service.classes;


import com.GestTacheCollab.GestTacheCollab.Reposity.UserReposity;
import com.GestTacheCollab.GestTacheCollab.Service.interfaces.UserService;
import org.springframework.stereotype.Service;
import com.GestTacheCollab.GestTacheCollab.Model.User;
import com.GestTacheCollab.GestTacheCollab.Model.Task;

@Service
public class UserServiceImpl implements UserService {
    private final UserReposity userReposity;

    public UserServiceImpl(UserReposity userReposity) {
        this.userReposity = userReposity;
    }

    /**
     * Inserts a new User into the database.
     *
     * @param user The User object to insert.
     */
    @Override
    public void insertUser(User user) {
        this.userReposity.save(user);
    }

    /**
     * Deletes a User from the database based on its ID.
     *
     * @param id The ID of the User to delete.
     */
    @Override
    public void deleteUser(Long id) {
        if (userReposity.existsById(id)) {
            this.userReposity.deleteById(id);
        }
    }

    /**
     * Updates an existing User in the database.
     *
     * @param id   The ID of the User to update.
     * @param user The updated User object.
     */
    @Override
    public void updateUser(Long id, User user) {
        User existingUser = userReposity.findById(id).orElse(null);
        if (existingUser != null) {
            existingUser.setName(user.getName());
            existingUser.setEmail(user.getEmail());
            existingUser.setPassword(user.getPassword());
            this.userReposity.save(existingUser);
        }
    }

    /**
     * Assigns a Task to a User.
     *
     * @param id   The ID of the User to assign the Task to.
     * @param task The Task to be assigned.
     */
    @Override
    public void assignTask(Long id, Task task) {
        User user = userReposity.findById(id).orElse(null);
        if (user != null) {
            task.setAssignedTo(user);
            this.userReposity.save(user);
        }
    }

    /**
     * Retrieves a User from the database based on its ID.
     *
     * @param id The ID of the User to retrieve.
     * @return The User object if found, or null if not found.
     */
    @Override
    public User getUserById(Long id) {
        return this.userReposity.findById(id).orElse(null);
    }
}
