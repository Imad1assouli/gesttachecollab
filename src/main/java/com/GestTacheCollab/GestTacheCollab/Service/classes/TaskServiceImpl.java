package com.GestTacheCollab.GestTacheCollab.Service.classes;

import com.GestTacheCollab.GestTacheCollab.Reposity.TaskRepository;
import com.GestTacheCollab.GestTacheCollab.Model.Task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import com.GestTacheCollab.GestTacheCollab.Service.interfaces.TaskService;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class TaskServiceImpl implements TaskService {
    private final TaskRepository taskRepository;


    public TaskServiceImpl(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    /**
     * Insert a task.
     *
     * @param task The task to insert.
     */
    @Override
    public void insertTask(Task task) {
        taskRepository.save(task);
    }

    /**
     * Delete a task by its ID.
     *
     * @param id The ID of the task to delete.
     */
    @Override
    public void deleteTask(Long id) {
        taskRepository.deleteById(id);
    }

    /**
     * Update a task by its ID.
     *
     * @param id   The ID of the task to update.
     * @param task The updated task.
     */
    @Override
    public void updateTask(Long id, Task task) {
        Optional<Task> existingTask = taskRepository.findById(id);
        if (existingTask.isPresent()) {
            Task taskToUpdate = existingTask.get();
            taskToUpdate.setTitle(task.getTitle());
            taskToUpdate.setDescription(task.getDescription());
            taskToUpdate.setStatus(task.getStatus());
            taskToUpdate.setDueDate(task.getDueDate());
            taskToUpdate.setCreatedBy(task.getCreatedBy());
            taskToUpdate.setAssignedTo(task.getAssignedTo());
            taskRepository.save(taskToUpdate);
        } else {
            log.info("Task does not exist");
        }
    }

    /**
     * Get all tasks.
     *
     * @return A list of tasks.
     */
    @Override
    public List<Task> getAllTasks() {
        return taskRepository.findAll();
    }

    /**
     * Get a task by its ID.
     *
     * @param id The ID of the task.
     * @return The task with the specified ID, or null if not found.
     */
    @Override
    public Task getTaskById(Long id) {
        return taskRepository.findById(id).orElse(null);
    }
}
