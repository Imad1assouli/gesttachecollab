package com.GestTacheCollab.GestTacheCollab;

import com.GestTacheCollab.GestTacheCollab.Contoller.UserController;
import com.GestTacheCollab.GestTacheCollab.Model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class GestTacheCollabApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestTacheCollabApplication.class, args);
	}

}




